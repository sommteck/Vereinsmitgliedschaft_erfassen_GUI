namespace Vereinsmitgliedschaft_erfassen_GUI
{
    partial class Vereinsmitgliedschaft_erfassen
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Vereinsmitgliedschaft_erfassen));
            this.label1 = new System.Windows.Forms.Label();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.maskedTextBox_birthday = new System.Windows.Forms.MaskedTextBox();
            this.rbtn_male = new System.Windows.Forms.RadioButton();
            this.rbtn_female = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl_euro_foerderbeitrag = new System.Windows.Forms.Label();
            this.txt_foerderbeitrag = new System.Windows.Forms.TextBox();
            this.rbtn_forderation = new System.Windows.Forms.RadioButton();
            this.rbtn_passive = new System.Windows.Forms.RadioButton();
            this.rbtn_active = new System.Windows.Forms.RadioButton();
            this.txtgroup_sports = new System.Windows.Forms.GroupBox();
            this.txt_wintersport = new System.Windows.Forms.TextBox();
            this.chk_Wintersport = new System.Windows.Forms.CheckBox();
            this.chk_Tennis = new System.Windows.Forms.CheckBox();
            this.chk_Fussball = new System.Windows.Forms.CheckBox();
            this.chk_handball = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.lbl_beitrag = new System.Windows.Forms.Label();
            this.btn_end = new System.Windows.Forms.Button();
            this.btn_ok = new System.Windows.Forms.Button();
            this.btn_clear = new System.Windows.Forms.Button();
            this.list_printers = new System.Windows.Forms.ListBox();
            this.btn_print = new System.Windows.Forms.Button();
            this.btn_print_preview = new System.Windows.Forms.Button();
            this.btn_print_dialog = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.groupBox1.SuspendLayout();
            this.txtgroup_sports.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(89, 31);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(100, 26);
            this.txt_name.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 19);
            this.label2.TabIndex = 2;
            this.label2.Text = "Geburtsdatum";
            // 
            // maskedTextBox_birthday
            // 
            this.maskedTextBox_birthday.Location = new System.Drawing.Point(144, 76);
            this.maskedTextBox_birthday.Mask = "00/00/0000";
            this.maskedTextBox_birthday.Name = "maskedTextBox_birthday";
            this.maskedTextBox_birthday.Size = new System.Drawing.Size(100, 26);
            this.maskedTextBox_birthday.TabIndex = 3;
            this.maskedTextBox_birthday.ValidatingType = typeof(System.DateTime);
            // 
            // rbtn_male
            // 
            this.rbtn_male.AutoSize = true;
            this.rbtn_male.Location = new System.Drawing.Point(32, 131);
            this.rbtn_male.Name = "rbtn_male";
            this.rbtn_male.Size = new System.Drawing.Size(62, 23);
            this.rbtn_male.TabIndex = 4;
            this.rbtn_male.TabStop = true;
            this.rbtn_male.Text = "Mann";
            this.rbtn_male.UseVisualStyleBackColor = true;
            // 
            // rbtn_female
            // 
            this.rbtn_female.AutoSize = true;
            this.rbtn_female.Location = new System.Drawing.Point(121, 131);
            this.rbtn_female.Name = "rbtn_female";
            this.rbtn_female.Size = new System.Drawing.Size(55, 23);
            this.rbtn_female.TabIndex = 5;
            this.rbtn_female.TabStop = true;
            this.rbtn_female.Text = "Frau";
            this.rbtn_female.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl_euro_foerderbeitrag);
            this.groupBox1.Controls.Add(this.txt_foerderbeitrag);
            this.groupBox1.Controls.Add(this.rbtn_forderation);
            this.groupBox1.Controls.Add(this.rbtn_passive);
            this.groupBox1.Controls.Add(this.rbtn_active);
            this.groupBox1.Location = new System.Drawing.Point(32, 176);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(399, 124);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mitgliedschaft";
            // 
            // lbl_euro_foerderbeitrag
            // 
            this.lbl_euro_foerderbeitrag.AutoSize = true;
            this.lbl_euro_foerderbeitrag.Enabled = false;
            this.lbl_euro_foerderbeitrag.Location = new System.Drawing.Point(270, 87);
            this.lbl_euro_foerderbeitrag.Name = "lbl_euro_foerderbeitrag";
            this.lbl_euro_foerderbeitrag.Size = new System.Drawing.Size(104, 19);
            this.lbl_euro_foerderbeitrag.TabIndex = 4;
            this.lbl_euro_foerderbeitrag.Text = "€ Förderbeitrag";
            this.lbl_euro_foerderbeitrag.Visible = false;
            // 
            // txt_foerderbeitrag
            // 
            this.txt_foerderbeitrag.Location = new System.Drawing.Point(152, 85);
            this.txt_foerderbeitrag.Name = "txt_foerderbeitrag";
            this.txt_foerderbeitrag.Size = new System.Drawing.Size(100, 26);
            this.txt_foerderbeitrag.TabIndex = 3;
            this.txt_foerderbeitrag.Visible = false;
            // 
            // rbtn_forderation
            // 
            this.rbtn_forderation.AutoSize = true;
            this.rbtn_forderation.Location = new System.Drawing.Point(20, 83);
            this.rbtn_forderation.Name = "rbtn_forderation";
            this.rbtn_forderation.Size = new System.Drawing.Size(115, 23);
            this.rbtn_forderation.TabIndex = 2;
            this.rbtn_forderation.TabStop = true;
            this.rbtn_forderation.Text = "Fördermitglied";
            this.rbtn_forderation.UseVisualStyleBackColor = true;
            this.rbtn_forderation.CheckedChanged += new System.EventHandler(this.rbtn_forderation_CheckedChanged);
            // 
            // rbtn_passive
            // 
            this.rbtn_passive.AutoSize = true;
            this.rbtn_passive.Location = new System.Drawing.Point(20, 54);
            this.rbtn_passive.Name = "rbtn_passive";
            this.rbtn_passive.Size = new System.Drawing.Size(65, 23);
            this.rbtn_passive.TabIndex = 1;
            this.rbtn_passive.TabStop = true;
            this.rbtn_passive.Text = "Passiv";
            this.rbtn_passive.UseVisualStyleBackColor = true;
            // 
            // rbtn_active
            // 
            this.rbtn_active.AutoSize = true;
            this.rbtn_active.Location = new System.Drawing.Point(20, 25);
            this.rbtn_active.Name = "rbtn_active";
            this.rbtn_active.Size = new System.Drawing.Size(60, 23);
            this.rbtn_active.TabIndex = 0;
            this.rbtn_active.TabStop = true;
            this.rbtn_active.Text = "Aktiv";
            this.rbtn_active.UseVisualStyleBackColor = true;
            this.rbtn_active.CheckedChanged += new System.EventHandler(this.rbtn_active_CheckedChanged);
            // 
            // txtgroup_sports
            // 
            this.txtgroup_sports.Controls.Add(this.txt_wintersport);
            this.txtgroup_sports.Controls.Add(this.chk_Wintersport);
            this.txtgroup_sports.Controls.Add(this.chk_Tennis);
            this.txtgroup_sports.Controls.Add(this.chk_Fussball);
            this.txtgroup_sports.Controls.Add(this.chk_handball);
            this.txtgroup_sports.Location = new System.Drawing.Point(32, 326);
            this.txtgroup_sports.Name = "txtgroup_sports";
            this.txtgroup_sports.Size = new System.Drawing.Size(275, 147);
            this.txtgroup_sports.TabIndex = 7;
            this.txtgroup_sports.TabStop = false;
            this.txtgroup_sports.Text = "Sportarten";
            this.txtgroup_sports.Visible = false;
            // 
            // txt_wintersport
            // 
            this.txt_wintersport.Location = new System.Drawing.Point(125, 109);
            this.txt_wintersport.Name = "txt_wintersport";
            this.txt_wintersport.Size = new System.Drawing.Size(134, 26);
            this.txt_wintersport.TabIndex = 4;
            this.txt_wintersport.Visible = false;
            // 
            // chk_Wintersport
            // 
            this.chk_Wintersport.AutoSize = true;
            this.chk_Wintersport.Location = new System.Drawing.Point(20, 112);
            this.chk_Wintersport.Name = "chk_Wintersport";
            this.chk_Wintersport.Size = new System.Drawing.Size(99, 23);
            this.chk_Wintersport.TabIndex = 3;
            this.chk_Wintersport.Text = "Wintersport";
            this.chk_Wintersport.UseVisualStyleBackColor = true;
            this.chk_Wintersport.CheckedChanged += new System.EventHandler(this.chk_Wintersport_CheckedChanged);
            // 
            // chk_Tennis
            // 
            this.chk_Tennis.AutoSize = true;
            this.chk_Tennis.Location = new System.Drawing.Point(20, 83);
            this.chk_Tennis.Name = "chk_Tennis";
            this.chk_Tennis.Size = new System.Drawing.Size(66, 23);
            this.chk_Tennis.TabIndex = 2;
            this.chk_Tennis.Text = "Tennis";
            this.chk_Tennis.UseVisualStyleBackColor = true;
            // 
            // chk_Fussball
            // 
            this.chk_Fussball.AutoSize = true;
            this.chk_Fussball.Location = new System.Drawing.Point(20, 54);
            this.chk_Fussball.Name = "chk_Fussball";
            this.chk_Fussball.Size = new System.Drawing.Size(73, 23);
            this.chk_Fussball.TabIndex = 1;
            this.chk_Fussball.Text = "Fußball";
            this.chk_Fussball.UseVisualStyleBackColor = true;
            // 
            // chk_handball
            // 
            this.chk_handball.AutoSize = true;
            this.chk_handball.Location = new System.Drawing.Point(20, 25);
            this.chk_handball.Name = "chk_handball";
            this.chk_handball.Size = new System.Drawing.Size(82, 23);
            this.chk_handball.TabIndex = 0;
            this.chk_handball.Text = "Handball";
            this.chk_handball.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(444, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 19);
            this.label3.TabIndex = 8;
            this.label3.Text = "Drucker auswählen";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.richTextBox1.Location = new System.Drawing.Point(667, 33);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(199, 282);
            this.richTextBox1.TabIndex = 9;
            this.richTextBox1.Text = "";
            this.richTextBox1.Visible = false;
            // 
            // lbl_beitrag
            // 
            this.lbl_beitrag.AutoSize = true;
            this.lbl_beitrag.Location = new System.Drawing.Point(458, 268);
            this.lbl_beitrag.Name = "lbl_beitrag";
            this.lbl_beitrag.Size = new System.Drawing.Size(0, 19);
            this.lbl_beitrag.TabIndex = 10;
            this.lbl_beitrag.Visible = false;
            // 
            // btn_end
            // 
            this.btn_end.BackColor = System.Drawing.Color.Red;
            this.btn_end.Location = new System.Drawing.Point(779, 449);
            this.btn_end.Name = "btn_end";
            this.btn_end.Size = new System.Drawing.Size(71, 40);
            this.btn_end.TabIndex = 11;
            this.btn_end.Text = "&Ende";
            this.btn_end.UseVisualStyleBackColor = false;
            this.btn_end.Click += new System.EventHandler(this.btn_end_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.BackColor = System.Drawing.Color.Green;
            this.btn_ok.Location = new System.Drawing.Point(336, 458);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(74, 31);
            this.btn_ok.TabIndex = 12;
            this.btn_ok.Text = "&OK";
            this.btn_ok.UseVisualStyleBackColor = false;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btn_clear
            // 
            this.btn_clear.BackColor = System.Drawing.Color.Yellow;
            this.btn_clear.Enabled = false;
            this.btn_clear.Location = new System.Drawing.Point(448, 458);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(75, 31);
            this.btn_clear.TabIndex = 13;
            this.btn_clear.Text = "&Löschen";
            this.btn_clear.UseVisualStyleBackColor = false;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // list_printers
            // 
            this.list_printers.FormattingEnabled = true;
            this.list_printers.ItemHeight = 19;
            this.list_printers.Location = new System.Drawing.Point(448, 78);
            this.list_printers.Name = "list_printers";
            this.list_printers.Size = new System.Drawing.Size(182, 137);
            this.list_printers.TabIndex = 14;
            // 
            // btn_print
            // 
            this.btn_print.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btn_print.Location = new System.Drawing.Point(382, 350);
            this.btn_print.Name = "btn_print";
            this.btn_print.Size = new System.Drawing.Size(76, 38);
            this.btn_print.TabIndex = 15;
            this.btn_print.Text = "&Drucken";
            this.btn_print.UseVisualStyleBackColor = false;
            this.btn_print.Click += new System.EventHandler(this.btn_print_Click);
            // 
            // btn_print_preview
            // 
            this.btn_print_preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btn_print_preview.Location = new System.Drawing.Point(480, 351);
            this.btn_print_preview.Name = "btn_print_preview";
            this.btn_print_preview.Size = new System.Drawing.Size(113, 37);
            this.btn_print_preview.TabIndex = 16;
            this.btn_print_preview.Text = "Druck&vorschau";
            this.btn_print_preview.UseVisualStyleBackColor = false;
            this.btn_print_preview.Click += new System.EventHandler(this.btn_print_preview_Click);
            // 
            // btn_print_dialog
            // 
            this.btn_print_dialog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btn_print_dialog.Location = new System.Drawing.Point(635, 351);
            this.btn_print_dialog.Name = "btn_print_dialog";
            this.btn_print_dialog.Size = new System.Drawing.Size(98, 37);
            this.btn_print_dialog.TabIndex = 17;
            this.btn_print_dialog.Text = "Druckdialo&g";
            this.btn_print_dialog.UseVisualStyleBackColor = false;
            this.btn_print_dialog.Click += new System.EventHandler(this.btn_print_dialog_Click);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // Vereinsmitgliedschaft_erfassen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(892, 535);
            this.Controls.Add(this.btn_print_dialog);
            this.Controls.Add(this.btn_print_preview);
            this.Controls.Add(this.btn_print);
            this.Controls.Add(this.list_printers);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.btn_end);
            this.Controls.Add(this.lbl_beitrag);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtgroup_sports);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.rbtn_female);
            this.Controls.Add(this.rbtn_male);
            this.Controls.Add(this.maskedTextBox_birthday);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_name);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Vereinsmitgliedschaft_erfassen";
            this.Text = "Vereinsmitgliedschaft erfassen";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.txtgroup_sports.ResumeLayout(false);
            this.txtgroup_sports.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox maskedTextBox_birthday;
        private System.Windows.Forms.RadioButton rbtn_male;
        private System.Windows.Forms.RadioButton rbtn_female;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txt_foerderbeitrag;
        private System.Windows.Forms.RadioButton rbtn_forderation;
        private System.Windows.Forms.RadioButton rbtn_passive;
        private System.Windows.Forms.RadioButton rbtn_active;
        private System.Windows.Forms.GroupBox txtgroup_sports;
        private System.Windows.Forms.TextBox txt_wintersport;
        private System.Windows.Forms.CheckBox chk_Wintersport;
        private System.Windows.Forms.CheckBox chk_Tennis;
        private System.Windows.Forms.CheckBox chk_Fussball;
        private System.Windows.Forms.CheckBox chk_handball;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label lbl_beitrag;
        private System.Windows.Forms.Button btn_end;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.ListBox list_printers;
        private System.Windows.Forms.Button btn_print;
        private System.Windows.Forms.Button btn_print_preview;
        private System.Windows.Forms.Button btn_print_dialog;
        private System.Windows.Forms.Label lbl_euro_foerderbeitrag;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.PrintDialog printDialog1;
    }
}

