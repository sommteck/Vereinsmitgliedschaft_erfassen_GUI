using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Printing; // zum Drucken

namespace Vereinsmitgliedschaft_erfassen_GUI
{
    public partial class Vereinsmitgliedschaft_erfassen : Form
    {

        private const char LF = (char)10;
        private double beitrag;

        List<string> Printers = new List<string>();

        public Vereinsmitgliedschaft_erfassen()
        {
            InitializeComponent();

            //Druckerauflistung anzeigen
            foreach(string p in PrinterSettings.InstalledPrinters)
            {
                Printers.Add(p);
                list_printers.Items.Add(p);
            }

        }

        private void btn_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBox1.Visible = true;
                btn_clear.Enabled = true;
                // Prüfen ob Mann oder Frau ausgewählt ist
                if (rbtn_male.Checked)
                    richTextBox1.Text = "Herr" + LF;
                if (rbtn_female.Checked)
                    richTextBox1.Text = "Frau" + LF;

                // Name und Geburtsdatum einlesen
                richTextBox1.Text += txt_name.Text + LF;
                richTextBox1.Text += maskedTextBox_birthday.Text + LF;

                // Mitgliedsart auswählen
                if (rbtn_active.Checked)
                {
                    richTextBox1.Text += "Aktives Mitglied" + LF;
                    beitrag = 100;
                    richTextBox1.Text += "Sportarten:" + LF;
                    // Sportarten auswählen
                    if (chk_Fussball.Checked)
                        richTextBox1.Text += "Fußball" + LF;
                    if (chk_handball.Checked)
                        richTextBox1.Text += "Handball" + LF;
                    if (chk_Tennis.Checked)
                        richTextBox1.Text += "Tennis" + LF;
                    if (chk_Wintersport.Checked)
                        richTextBox1.Text += "Wintersport(en):" + LF + txt_wintersport.Text + LF;
                }

                if (rbtn_passive.Checked)
                {
                    richTextBox1.Text += "Passives Mitglied" + LF;
                    beitrag = 50;
                }
                if (rbtn_forderation.Checked)
                {
                    richTextBox1.Text += "Fördermitglied" + LF;
                    beitrag = 50 + Convert.ToDouble(txt_foerderbeitrag.Text);
                }
                lbl_beitrag.Visible = lbl_euro_foerderbeitrag.Visible = true;
                lbl_beitrag.Text = "Mitgliedsbeitrag: " + beitrag.ToString("C");
                richTextBox1.Text += "Mitgliedsbeitrag: " + beitrag.ToString("C") + LF;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                txt_foerderbeitrag.Clear();
                txt_foerderbeitrag.Focus();
            }
        }

        // wenn Radiobutton Aktiv aktiviert
        private void rbtn_active_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtn_active.Checked)
                txtgroup_sports.Visible = true;
            else
                txtgroup_sports.Visible = false;
        }

        private void rbtn_forderation_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtn_forderation.Checked)
            {
                txt_foerderbeitrag.Visible = lbl_beitrag.Visible = lbl_euro_foerderbeitrag.Visible = true;
                txt_foerderbeitrag.Focus();
            }
            else
                txt_foerderbeitrag.Visible = lbl_beitrag.Visible = false;
        }

        private void chk_Wintersport_CheckedChanged(object sender, EventArgs e)

        // Wenn Radiobutton Wintersport aktiviert

        {
            if (chk_Wintersport.Checked)
            {
                txt_wintersport.Visible = true;
                txt_wintersport.Text = null;
                txt_wintersport.Focus();
            }
            else
                txt_wintersport.Visible = false;
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            btn_clear.Enabled = false;
            txt_foerderbeitrag.Text = txt_name.Text = txt_wintersport.Text = lbl_beitrag.Text =
                maskedTextBox_birthday.Text = richTextBox1.Text = null;
            rbtn_active.Checked = rbtn_forderation.Checked = rbtn_passive.Checked = rbtn_male.Checked =
                rbtn_female.Checked = richTextBox1.Visible = lbl_euro_foerderbeitrag.Visible = false;
            chk_Fussball.Checked = chk_handball.Checked = chk_Tennis.Checked = chk_Wintersport.Checked = false;
            txt_name.Focus();
        }

        private void btn_print_Click(object sender, EventArgs e)
        {
            try
            {
                PrintDocument printdoc = new PrintDocument();   // Instanzieren des Druckobjekts

                printdoc.PrinterSettings.PrinterName = PrinterSettings.InstalledPrinters[list_printers.SelectedIndex];
                printdoc.PrintPage += new PrintPageEventHandler(printDocument1_PrintPage);
                printdoc.Print();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawString(richTextBox1.Text, new Font("Helvetica Neue", 14),
            new SolidBrush(Color.Black), new Point(45,45));
        }

        private void btn_print_preview_Click(object sender, EventArgs e)
        {
            try
            {
                PrintDocument printdoc = new PrintDocument();   // Instanzieren des Druckobjekts
                // Drucker auswählen
                printdoc.PrinterSettings.PrinterName = PrinterSettings.InstalledPrinters[list_printers.SelectedIndex];
                printdoc.PrintPage += new PrintPageEventHandler(printDocument1_PrintPage);
                // Druckdokument dem Previewsteuerelement zuweisen und drucken
                printPreviewDialog1.Document = printdoc;
                if (printPreviewDialog1.ShowDialog() == DialogResult.OK)
                    printdoc.Print();
            }

            catch
            {
                MessageBox.Show("Bitte zuerst einen Drucker auswählen");
            }
        }

        private void btn_print_dialog_Click(object sender, EventArgs e)
        {
            PrintDocument printdoc = new PrintDocument();   // Instanzieren des Druckobjekts
            // Drucker auswählen
            printdoc.PrintPage += new PrintPageEventHandler(printDocument1_PrintPage);
            // Druckdokument dem Dialogsteuerelement zuweisen und drucken
            printDialog1.Document = printdoc;
            if (printDialog1.ShowDialog() == DialogResult.OK)
                printdoc.Print();
        }
    }
}